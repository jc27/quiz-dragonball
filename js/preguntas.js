function getQuestions() {

  // Mock data
  const questions = [
    {
      text: '1.¿Cual es el nombre real de Goku?',
      image: 'img/question1.jpg',
      answers: [
          'Kakaroto',
          'Bob',
          'Nappa',
          'Bardock'
      ],
      correct: 0
    },
    {
    text: '2.De las esferas del dragon. ¿Cual es el numero de la suerte de Goku?',
    image: 'img/question2.jpg',
    answers: [
        '5',
        '13',
        '4',
        '6'
    ],
    correct: 2
    },
    {
    text: '3.¿Quien mato a Cell?',
    image: 'img/question3.png',
    answers: [
        'Goku',
        'Vegeta',
        'Gohan',
        'Trunks'
    ],
    correct: 2
    },
    {
    text: '4.A cuales dos Super Sayayin absorbió Buu?',
    image: 'img/question4.jpg',
    answers: [
        'Goku,Gohan',
        'Gotenks,Gohan',
        'Michael Jackson,Vegeta',
        'Broly,Bardock'
    ],
    correct: 1
    },
    {
    text: '5.¿A quien mato Frezer para que Goku se transformara en Super Sayayin?',
    image: 'img/question5.jpg',
    answers: [
        'Gohan',
        'Piccolo',
        'Krillin',
        'Chi Chi'
    ],
    correct: 2
    },
    {
    text: '6.¿Como se llama la esposa de Goku?',
    image: 'img/question6.jpg',
    answers: [
        'Bulma',
        'Chi Chi',
        'Misty',
        'Maron'
    ],
    correct: 1
    },
    {
    text: '7.¿Como se llama el Dragon en el planeta Namekusei?',
    image: 'img/question7.jpg',
    answers: [
        'Omega Shenlong',
        'Porunga',
        'Shenlong',
        'Billy el niño'
    ],
    correct: 1
    },
    {
    text: '8.Que es Bardock de Goku?',
    image: 'img/question8.jpg',
    answers: [
        'Hermano',
        'Cousin',
        'Papa',
        'Enemigo'
    ],
    correct: 2
    },
    {
    text: '9.Cual Sayayin derroto al Androide # 19?',
    image: 'img/question9.jpg',
    answers: [
        'Gohan',
        'Trunks',
        'Vegeta',
        'Goku'
    ],
    correct: 2
    },
  ]

// Return data
return questions
}