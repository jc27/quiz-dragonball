# webdevelopment

Juego tipo pregunta / respuesta basado en Dragon Ball de 9 preguntas.

Organizado por carpetas / ficheros:

**carpeta JS** -> esta el contenido de JavaScript.
**carpeta CSS** -> estan los estilos.
**carpeta img** -> donde tengo almacenadas las imágenes.
**questions.js** -> donde esta la información sobre las preguntas, imágenes, solución correcta y la funcion para mostrar la información.
**main.js** -> donde estan definidas las variables y la lógica a seguir, pinta los elementos en el DOM y ejecuta las funciones.


**Logica programacion:**

Estados de la aplicación
**Inicio** - Estado de espera para interaccion con usuario primer click.
**Juego** - 9 Preguntas tipo test que permite cambiar de opción y por color
**Fin** - Puntuacíon y opción a intentarlo de nuevo.

Eventos - clicks de cliente
Trigger - los botones (colores redBtn blueBtn, nextBtn)
Target - el cambio de color del boton al click (son elementos del html junto al trigger)
Handler - función manejadora que reacciona ante los eventos (en este caso clicks). Ejemplo: selectAnswer() 
